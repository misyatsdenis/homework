import React, { useEffect, useState } from 'react'
import WishList from '../components/WishList/WishList'

let FavoritePage = () => {
	let [WishArray, setWishArray] = useState(
		JSON.parse(localStorage.getItem('WISHARRAY'))
	)
	let [Wish, setWish] = useState(WishArray ? WishArray : [])

	let RemoveProd = (event) => {
		let removeCardId = event.target.closest('.wish-card').id
		let removeCarIndex = Wish.findIndex(function (element) {
			return element.id == removeCardId
		})
		Wish.splice(removeCarIndex, 1)
		localStorage.setItem(`WISHARRAY`, JSON.stringify(Wish))
		setWishArray(JSON.parse(localStorage.getItem('WISHARRAY')))
	}
	useEffect(() => {
		setWish(WishArray)
	}, [])
	useEffect(() => {}, [Wish])

	return (
		<div className="WishPage">
			<div className="WishPage-wrap">
				<div className="product-container">
					<h1>that's what you want</h1>
					<WishList RemoveProd={RemoveProd} Wish={Wish} />
				</div>
			</div>
		</div>
	)
}
export default FavoritePage
