import React, { Component } from 'react'
import Button from '../../../Buttons/Button'
import Modal from '../../../Modal/Modal'
import { AiOutlineStar } from 'react-icons/ai'
import { AiFillStar } from 'react-icons/ai'
import './ProductCard.scss'

let ProductCard = (props) => {
	let { products, onAddBUYARRAY, onAddWISHARRAY } = props
	let StarInfoLocal = JSON.parse(localStorage.getItem(`Star ${props.cardId}`))

	let [modalBuy, setmodalBuy] = React.useState(false)
	let [starFill, setStarFill] = React.useState(StarInfoLocal)

	let addLikeArray = (event) => {
		let cardID = event.target.closest('.product-card').id
		let card = products.find((item) => item.id == cardID)
		console.log('add', card)
		localStorage.setItem(cardID, JSON.stringify(card))
	}
	let removeLikeArray = (event) => {
		let cardID = event.target.closest('.product-card').id
		localStorage.removeItem(cardID)
	}

	let show = () => {
		setmodalBuy(true)
	}
	const starClick = (e) => {
		e.preventDefault()
		setStarFill((s) => !s)
	}
	React.useEffect(() => {
		localStorage.setItem(`Star ${props.cardId}`, JSON.stringify(starFill))
	})

	return (
		<div className="product-card" id={props.cardId}>
			<div className="product-top-content">
				<div className="product-image_wrap">
					<img src={props.imageUrl} className="product-image" />
				</div>
				<div className="product-wish-content">
					<a className="nav-item-link" href="" onClick={starClick}>
						{starFill ? (
							<AiFillStar
								className="star-icon"
								onClick={removeLikeArray}
							/>
						) : (
							<AiOutlineStar
								className="star-icon"
								onClick={(event) => {
									onAddWISHARRAY(event)
									addLikeArray(event)
								}}
							/>
						)}
					</a>
					<h3 className="aside-text">Dota2</h3>
				</div>
			</div>
			<div className="info">
				<h3>
					{' '}
					<span>Name hero </span>
					<br />
					{props.name}
				</h3>
				<p className="price">
					<span>Cost:</span> {props.price} UAH
				</p>
				<p className="color">
					<span>Main color hero: </span>
					{props.color}
				</p>
			</div>

			<Button text="Add to cart" onClick={show} color="red" />
			<React.Fragment>
				<Modal
					isModalShow={modalBuy}
					isModalHide={() => setmodalBuy(false)}
					closeButton={true}
					backgroundColor="linear-gradient(to top, #8e0e00, #1f1c18)"
					header="Do you want to buy a product?"
					text={props.name}
					// onAddBUYARRAY={onAddBUYARRAY}
					onClick={onAddBUYARRAY}
				/>
			</React.Fragment>
		</div>
	)
}

export default ProductCard
