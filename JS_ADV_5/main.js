let findAPI = 'https://api.ipify.org/?format=json'
let ServiceIP = 'http://ip-api.com'
let nededFields =
	'fields=status,message,continent,country,regionName,city,district'
let container = document.querySelector('.containerIP')
let button = document.querySelector('.btn')

button.addEventListener('click', async (event) => {
	event.preventDefault()
	let IP = await findIP()
	let adresUser = await findAdres(IP)
	let { continent, country, regionName, city, district } = adresUser
	let renderAdres = await renderIP(
		continent,
		country,
		regionName,
		city,
		district
	)
	container.insertAdjacentHTML('afterbegin', renderAdres)
})

async function findIP() {
	try {
		let responseURL = await fetch(findAPI)
		let resultFetch = await responseURL.json()
		// console.log(resultFetch.ip)
		return resultFetch.ip
	} catch (e) {
		console.error(e)
	}
}

async function findAdres(IP) {
	try {
		console.log(IP)
		let responseURL = await fetch(
			ServiceIP + '/json/' + IP + '?' + nededFields
		)
		let resultFetch = await responseURL.json()
		console.log(resultFetch)
		return resultFetch
	} catch (e) {
		console.error(e)
	}
}

async function renderIP(continent, country, region, city, district) {
	try {
		let aderesCard = `<div class="adres-card">
		<h2 class="continent">${continent}</h2>
		<p class = "country">${country}</p>
		<p class = "region">${region}</p>
		<p class = "city">${city}</p>
		<p class = "district">${district}</p>
		</div>`
		return aderesCard
	} catch (e) {
		console.error(e)
	}
}
