import React from 'react'
import { GiSwapBag } from 'react-icons/gi'
import { useSelector } from 'react-redux'

import './BasketIcon.scss'
let BasketIcon = () => {
	let basketArray = useSelector((state) => state.basket)

	return (
		<div className="basket">
			<GiSwapBag size={35} className="basket-icon" />
			{basketArray.length>0?<span>{basketArray.length}</span>:null}
			
		</div>
	)
}
export default BasketIcon

