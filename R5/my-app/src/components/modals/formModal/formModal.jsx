import React from 'react'
import * as Yup from 'yup'
import { useFormik } from 'formik'
import { useDispatch } from 'react-redux'
import { closeByuModal, orderModal, clearBasket } from '../../../store/actions'
import './formModal.scss'

let FormModal = () => {
	let dispatch = useDispatch()

	let closeModal = () => {
		dispatch(closeByuModal())
	}

	let openOrderModal = (values) => {
		dispatch(orderModal(values))
		dispatch(clearBasket())
	}

	let formik = useFormik({
		initialValues: {
			firstName: '',
			lastName: '',
			age: '',
			adress: '',
			telephone: '',
		},
		validationSchema: Yup.object({
			firstName: Yup.string()
				.max(15, 'Имя не больше 15 символов пожалуйсто')
				.required('Обезательно заполни а то кому отдать?'),
			lastName: Yup.string()
				.max(15, 'Фамилия не больше 15 символов пожалуйсто')
				.required('Обезательно заполни а то кому отдать?'),
			adress: Yup.string()
				.max(15, 'адрес не больше 15 символов пожалуйсто')
				.required('Обезательно заполни а то другой заберет'),
			age: Yup.number()
				.min(2, 'Детям сюда нельзя, зови родителей')
				.required('Не стесняйся, нам нужен твой возраст'),
			telephone: Yup.number()
				.min(7, 'Городские номера нам не интересны')
				.required('Оператору звонить в трубу? так что пиши давай'),
		}),

		onSubmit: (values) => {
			closeModal()
			openOrderModal(values)
		},
	})

	return (
		<div
			className="form-container"
			onClick={() => {
				closeModal()
			}}
		>
			<form
				onSubmit={formik.handleSubmit}
				onReset={formik.handleReset}
				className="form"
				onClick={(e) => {
					e.stopPropagation()
				}}
			>
				<h2>Реквизиты для покупки</h2>
				<div className="input-container">
					<input
						id="firstName"
						name="firstName"
						type="text"
						placeholder="First Name"
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
						value={formik.values.firstName}
					/>
					{formik.touched.firstName && formik.errors.firstName ? (
						<p>{formik.errors.firstName}</p>
					) : null}
				</div>
				<div className="input-container">
					<input
						id="lastName"
						name="lastName"
						type="text"
						placeholder="Last Name"
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
						value={formik.values.lastName}
					/>
					{formik.touched.lastName && formik.errors.lastName ? (
						<p>{formik.errors.lastName}</p>
					) : null}
				</div>
				<div className="input-container">
					<input
						id="age"
						name="age"
						type="text"
						placeholder="Age"
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
						value={formik.values.age}
					/>
					{formik.touched.age && formik.errors.age ? <p>{formik.errors.age}</p> : null}
				</div>
				<div className="input-container">
					<input
						id="adress"
						name="adress"
						type="text"
						placeholder="adress"
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
						value={formik.values.adress}
					/>
					{formik.touched.adress && formik.errors.adress ? (
						<p>{formik.errors.adress}</p>
					) : null}
				</div>
				<div className="input-container">
					<input
						id="telephone"
						name="telephone"
						type="tel"
						placeholder="telephone"
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
						value={formik.values.telephone}
					/>
					{formik.touched.telephone && formik.errors.telephone ? (
						<p>{formik.errors.telephone}</p>
					) : null}
				</div>
				<div className="btn-container">
					<button type="submit">Checkout</button>
					<button type="reset">Reset</button>
					<button
						onClick={() => {
							closeModal()
						}}
					>
						Закрыть
					</button>
				</div>
			</form>
		</div>
	)
}

export default FormModal
