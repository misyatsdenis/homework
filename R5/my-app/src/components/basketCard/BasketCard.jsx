import React from 'react'
import './BasketCard.scss'
import { FaTrashAlt } from 'react-icons/fa'
// import { removeProguctFromBasket } from '../../store/actions'
import { DeleteModal } from '../../store/actions'
import { useDispatch } from 'react-redux'

let BasketCard = (props) => {
	let { item } = props
	let dispatch = useDispatch()

	let openDeleteModal = () => {
		dispatch(DeleteModal(item))
	}
	return (
		<div className="basket-card" id={item.id}>
			<div className="wrap">
				<div className="content">
					<div className="img-wrap">
						<img src={item.imageUrl} alt="asd"></img>
					</div>
					<div className="basket-info">
						<p>{item.info}</p>
					</div>
					<div className="addRemoveBtn-wrap">
						<div className="addRemoveBtn">
							<button className="addQti">+</button>
							<input type="text" value="1"></input>
							<button className="removeQti">-</button>
						</div>
						<div className="price-wrap">
							<p className="price-curent">
								Unit Price: {item.price} $
							</p>
							<p className="price-total">
								Total Price: <span>{item.price} </span>$
							</p>
						</div>
					</div>
					<div className="button-trash">
						<FaTrashAlt
							className="trashIcon"
							onClick={openDeleteModal}
						/>
					</div>
				</div>
			</div>
		</div>
	)
}

export default BasketCard
