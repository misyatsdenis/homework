import React from 'react'
import ProductList from '../components/ProductList/ProductList'
import getProguct from '../components/api/get'

let HomePage = () => {
	let BUYARRAYLocal = JSON.parse(localStorage.getItem(`BUYARRAY`))
	let WISHARRAYLocal = JSON.parse(localStorage.getItem(`WISHARRAY`))
	console.log(BUYARRAYLocal)
	let [BUYARRAY, setBUYARRAY] = React.useState(
		BUYARRAYLocal ? BUYARRAYLocal : []
	)
	let [WISHARRAY, setWISHARRAY] = React.useState(
		WISHARRAYLocal ? WISHARRAYLocal : []
	)
	let [products, setProducts] = React.useState([])

	let onAddBUYARRAY = (event) => {
		let cardID = event.target.closest('.product-card').id
		let card = products.find((item) => item.id == cardID)
		setBUYARRAY([...BUYARRAY, card])
	}
	React.useEffect(() => {
		localStorage.setItem(`BUYARRAY`, JSON.stringify(BUYARRAY))
	}, [BUYARRAY])

	let onAddWISHARRAY = (event) => {
		let cardID = event.target.closest('.product-card').id
		let card = products.find((item) => item.id == cardID)
		setWISHARRAY([...WISHARRAY, card])
	}

	React.useEffect(() => {
		localStorage.setItem(`WISHARRAY`, JSON.stringify(WISHARRAY))
	}, [WISHARRAY])

	React.useEffect(() => {
		getProguct().then((products) => setProducts(products))
	}, [])
	React.useEffect(() => {}, [products])

	return (
		<div>
			<ProductList
				onAddBUYARRAY={onAddBUYARRAY}
				onAddWISHARRAY={onAddWISHARRAY}
				BUYARRAY={BUYARRAY}
				products={products}
			/>
		</div>
	)
}

export default HomePage
