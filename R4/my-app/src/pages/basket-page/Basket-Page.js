import React from 'react'
import BasketList from '../../components/basketList/BasketList'
import PriceBlock from '../../components/priceBlock/PriceBlock'
import "./Basket-Page.scss"

let BasketPage = () => {
	return (
		<div className="BasketPage">
			<div className="BasketPage-wrap">
				<div className="product-container">
					<BasketList />
				</div>
				<PriceBlock />
			</div>
		</div>
	)
}
export default BasketPage

// let [BasketArray2, setBasketArray2] = useState(
// 	JSON.parse(localStorage.getItem('BUYARRAY'))
// )
// let [Basket, setBasket] = useState(BasketArray2 ? BasketArray2 : [])

// let RemoveProd = (event) => {
// 	let removeCardId = event.target.closest('.basket-card').id
// 	console.log('removeCardId', removeCardId)
// 	let removeCarIndex = Basket.findIndex(function (element) {
// 		return element.id == removeCardId
// 	})
// 	console.log('removeCarIndex', removeCarIndex)
// 	Basket.splice(removeCarIndex, 1)
// 	console.log('Basket', Basket)
// 	localStorage.setItem(`BUYARRAY`, JSON.stringify(Basket))
// 	setBasketArray2(JSON.parse(localStorage.getItem('BUYARRAY')))
// }

// useEffect(() => {
// 	setBasket(BasketArray2)
// }, [])
// useEffect(() => {}, [Basket])
