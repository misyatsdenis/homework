import React, { useState } from 'react'
import Button from './components/Buttons/Button'
import Modal from './components/Modal/Modal'

let App = (props) => {
	let [isModalShow, setIsModalShow] = useState({
		modal1: false,
		modal2: false,
	})

	return (
		<div>
			<Button
				text="Open first modal"
				backgroundColor="red"
				onClick={() => setIsModalShow({ ...isModalShow, modal1: true })}
				className="modal-btn first "
			/>

			<Button
				text="Open second modal"
				backgroundColor="blue"
				onClick={() => setIsModalShow({ ...isModalShow, modal2: true })}
				className="modal-btn second "
			/>
			<>
				<Modal
					isModalShow={isModalShow.modal1}
					closeButton={true}
					backgroundColor="green"
					header="Do you want...."
					isModalHide={() =>
						setIsModalShow({ ...isModalShow, modal1: false })
					}
					text="Test text"
				/>

				<Modal
					isModalShow={isModalShow.modal2}
					isModalHide={() =>
						setIsModalShow({ ...isModalShow, modal2: false })
					}
					closeButton={false}
					backgroundColor="#c6ffdd"
					header="Lorem"
					text="Lorem"
				/>
			</>
		</div>
	)
}

export default App
