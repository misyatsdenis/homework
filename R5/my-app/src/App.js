import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { HomePage } from './pages/home-page/HomePage'
import BasketPage from './pages/basket-page/Basket-Page'
import FavoritePage from './pages/favorite-page/FavoritePage'
import Headar from './components/header/Header'
function App() {
	return (
		<Router>
			<div className="page-wrap">
				<Headar />
				<Switch>
					<Route exact path="/">
						<HomePage />
					</Route>
					<Route exact path="/favorites">
						<FavoritePage />
					</Route>
					<Route exact path="/basket">
						<BasketPage />
					</Route>
				
				</Switch>
			</div>
		</Router>
	)
}

export default App
