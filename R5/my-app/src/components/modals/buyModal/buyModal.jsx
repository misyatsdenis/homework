import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { setProguctToBasket, closeByuModal } from '../../../store/actions'
import Button from '../../button/Button'
// import './Modal.scss'

let BuyModal = () => {
	let BuyModal = useSelector((state) => state.Modal)

	let dispatch = useDispatch()

	let closeModal = () => {
		dispatch(closeByuModal())
	}

	let addToBasket = () => {
		dispatch(setProguctToBasket(BuyModal.info))
		dispatch(closeByuModal())
	}

	return (
		<div
			className="modal-container"
			onClick={() => {
				closeModal()
			}}
		>
			<div className="modal" onClick={(e) => e.stopPropagation()}>
				<div className="modal-header">
					<h3>Добавить товар в корзину?</h3>
				</div>
				<div className="modal-body">
					<p>{BuyModal.info.name}</p>
				</div>
				<div className="modal-btn-content">
					<Button onClick={addToBasket}>
						Да <br /> хочу купить
					</Button>
					<Button
						onClick={() => {
							closeModal()
						}}
					>
						Нет <br /> пожалуй откажусь
					</Button>
				</div>
			</div>
		</div>
	)
}
export default BuyModal
