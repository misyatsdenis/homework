let filmsUrl = 'https://ajax.test-danit.com/api/swapi/films'
let button = document.querySelector('.btn')
async function findFilms() {
	try {
		let responseURL = await fetch(filmsUrl)
		let resultFetch = await responseURL.json()
		return resultFetch
	} catch (e) {
		console.error(e)
	}
}

async function findActor(characters) {
	try {
		let ActList = document.querySelector('.film-description')
		for (let i = 0; i < characters.length; i++) {
			let actorUrl = characters[i]
			let responseURL = await fetch(actorUrl)
			let resultFetch = await responseURL.json()
			let actorName = resultFetch.name
			ActList.insertAdjacentHTML('beforeend', `<li>${actorName}</li>`)
		}
		return ActList
	} catch (e) {
		console.error(e)
	}
}

async function renderFilm(container, episodeId, name, openingCrawl) {
	try {
		let filmCard = `<div class="film-card">
		<h2 class="film-name">${episodeId}.  ${name}</h2>
		<div class="film-description">${openingCrawl}</div>
		</div>`
		container.insertAdjacentHTML('afterbegin', filmCard)
	} catch (e) {
		console.error(e)
	}
}
button.addEventListener('click', async (event) => {
	event.preventDefault()
	let filmList = await findFilms()
	let container = document.querySelector('.filmsList')
	for (let i = 0; i < filmList.length; i++) {
		let { episodeId, name, openingCrawl, characters } = filmList[i]
		renderFilm(container, episodeId, name, openingCrawl)
		findActor(characters)
	}
})
