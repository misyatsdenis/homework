import React from 'react'
import './PriceBlock.scss'

let PriceBlock = (props) => {
	let { Basket } = props
	return (
		<div className="price-container">
			<aside>
				<h2>Summary</h2>
				<div className="items">
					<p>Items</p>
					<p>1</p>
				</div>
				<div className="subtotal">
					<p>Subtotal</p>
					<p>50,5 $</p>
				</div>
				<div className="promopcod">
					<p>PromoCode</p>
					<input type="text"></input>
				</div>
				<div className="line"></div>
				<div className="total">
					<p>Total</p>
					<p>100 $</p>
				</div>
				<div className="btnBuy">
					<button type="submit">Buy</button>
				</div>
			</aside>
		</div>
	)
}
export default PriceBlock
