import React from 'react'
import BasketList from '../../components/basketList/BasketList'
import PriceBlock from '../../components/priceBlock/PriceBlock'
import "./Basket-Page.scss"

let BasketPage = () => {
	return (
		<div className="BasketPage">
			<div className="BasketPage-wrap">
				<div className="product-container">
					<BasketList />
				</div>
				<PriceBlock />
			</div>
		</div>
	)
}
export default BasketPage
