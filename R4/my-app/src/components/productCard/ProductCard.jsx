import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
// import Button from '../button/Button'

import ProductInfo from '../productInfo/ProductInfo'
import { AiOutlineStar, AiFillStar } from 'react-icons/ai'
import { setProguctToFavorite } from '../../store/actions'
import { existItemInBasket } from '../../utils/qwe'
import './ProductCard.scss'

let ProductCard = ({ product }) => {
	let itemsInFav = useSelector((state) => state.favorites)

	let dispatch = useDispatch()

	let toggleFavorite = () => {
		dispatch(setProguctToFavorite(product))
	}

	return (
		<div className="product-card" id={product.id}>
			<div className="product-top-content">
				<div className="product-image_wrap">
					<img
						src={product.imageUrl}
						className="product-image"
						alt="asd"
					/>
				</div>
				<div className="product-wish-content">
					<span className="nav-item-link">
						{existItemInBasket(itemsInFav, product.id) ? (
							<AiFillStar onClick={toggleFavorite} />
						) : (
							<AiOutlineStar onClick={toggleFavorite} />
						)}
					</span>
					<h3 className="aside-text">Dota2</h3>
				</div>
			</div>
			<ProductInfo product={product} />
		</div>
	)
}

export default ProductCard
