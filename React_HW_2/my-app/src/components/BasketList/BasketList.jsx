import React from 'react'
import './BasketList.scss'
import BasketCard from './components/BasketCard/BasketCard'
let BasketList = (props) => {
	let { Basket, RemoveProd } = props

	return (
		<div className="basket-product-list">
			{Basket.map((item) => (
				<BasketCard key={item.id} item={item} RemoveProd={RemoveProd} Basket={Basket} />
			))}
		</div>
	)
}
export default BasketList
