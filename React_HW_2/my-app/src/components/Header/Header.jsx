import React from 'react'
import { AiOutlineStar } from 'react-icons/ai'
import { GiSwapBag } from 'react-icons/gi'
import { Link } from 'react-router-dom'
import './Header.scss'

const Header = (props) => {
	return (
		<header>
			<h1 className="logo-title">
				<Link to="/" className="nav-item-link">
					<img
						className="logo-img"
						src="https://cdn.cloudflare.steamstatic.com/apps/dota2/images/nav/logo.png"
					/>
				</Link>
			</h1>

			<nav className="main-nav">
				<ul className="nav-list">
					<li className="nav-item">
						<Link to="/" className="nav-item-link">
							Home
						</Link>
					</li>
					<li className="nav-item">
						<Link to="/guide" className="nav-item-link">
							guide
						</Link>
					</li>
					<li className="nav-item">
						<Link to="/about" className="nav-item-link">
							About us
						</Link>
					</li>
					<li className="nav-item">
						<Link to="/favorites" className="nav-item-link">
							<AiOutlineStar className="star-icon" />
						</Link>
					</li>
					<li className="nav-item">
						<Link to="/basket" className="nav-item-link">
							<GiSwapBag className="star-icon" />
						</Link>
					</li>
				</ul>
			</nav>
		</header>
	)
}

export default Header
