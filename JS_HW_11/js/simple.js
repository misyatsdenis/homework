let btn = document.querySelectorAll('button')
console.log(btn)

document.body.addEventListener('keypress', function (event) {
	btn.forEach((element) => {
		if (element.classList.contains('active')) {
			element.classList.remove('active')
		}
		if (element.innerText.toLowerCase() === event.key.toLowerCase()) {
			element.classList.add('active')
			console.log(element)
		}
	})
})
