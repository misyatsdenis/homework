import React from 'react'
import FavoriteList from '../../components/favoriteList/FavoriteList'

let FavoritePage = () => {
	return (
		<div className="WishPage">
			<div className="WishPage-wrap">
				<div className="product-container">
					<h1>that's what you want</h1>
					<FavoriteList />
				</div>
			</div>
		</div>
	)
}
export default FavoritePage
