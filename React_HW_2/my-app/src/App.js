import React from 'react'

import Headar from '../src/components/Header/Header'
import './scss/main.scss'

// import { HomePage, BasketPage, FavoritePage } from './pages'
import HomePage from './pages/Home'
import BasketPage from './pages/Basket'
import FavoritePage from './pages/Favorites'

import { Switch, Route } from 'react-router-dom'

let App = (props) => {
	return (
		<div className="page-wrap">
			<Headar />
			<Switch>
				<Route exact path="/" component={HomePage} />
				<Route exact path="/basket" component={BasketPage} />
				<Route exact path="/favorites" component={FavoritePage} />
			</Switch>
		</div>
	)
}

export default App
