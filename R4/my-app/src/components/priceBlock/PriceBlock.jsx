import React from 'react'
import { useSelector } from 'react-redux'
import { summBasket } from '../../utils/qwe'

import './PriceBlock.scss'

let PriceBlock = (props) => {
	const BasketArray = useSelector((state) => state.basket)
	return (
		<div className="price-container">
			<aside>
				<h2>Summary</h2>
				<div className="items">
					<p>Items</p>
					<p>{BasketArray.length}</p>
				</div>
				<div className="subtotal">
					<p>Subtotal</p>
					<p>{summBasket(BasketArray)} $</p>
				</div>
				<div className="promopcod">
					<p>PromoCode</p>
					<input type="text"></input>
				</div>
				<div className="line"></div>
				<div className="total">
					<p>Total</p>
					<p>{summBasket(BasketArray)} $</p>
				</div>
				<div className="btnBuy">
					<button type="submit">Buy</button>
				</div>
			</aside>
		</div>
	)
}
export default PriceBlock
