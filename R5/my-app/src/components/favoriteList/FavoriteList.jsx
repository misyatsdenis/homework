import React from 'react'
import FavoriteCard from '../favoriteCard/FavoriteCard'
import { useSelector } from 'react-redux'
import './FavoriteList.scss'

let FavoriteList = () => {
	const FavoriteArray = useSelector((state) => state.favorites)

	return (
		<div className="wish-list">
			{FavoriteArray.map((product) => (
				<FavoriteCard key={product.id} product={product}  />
			))}
		</div>
	)
}

export default FavoriteList
