const books = [
	{
		author: 'Скотт Бэккер',
		name: 'Тьма, что приходит прежде',
		price: 70,
	},
	{
		author: 'Скотт Бэккер',
		name: 'Воин-пророк',
	},
	{
		name: 'Тысячекратная мысль',
		price: 70,
	},
	{
		author: 'Скотт Бэккер',
		name: 'Нечестивый Консульт',
		price: 70,
	},
	{
		author: 'Дарья Донцова',
		name: 'Детектив на диете',
		price: 40,
	},
	{
		author: 'Дарья Донцова',
		name: 'Дед Снегур и Морозочка',
	},
]
let neededKeys = ['author', 'name', 'price']
let filterArr = []

let elRoot = document.getElementById('root')
let newTagUl = document.createElement('ul')
elRoot.appendChild(newTagUl)

function checking(arr) {
	for (let i = 0; i < arr.length; i++) {
		try {
			if (neededKeys.every((key) => Object.keys(arr[i]).includes(key))) {
				console.log(arr[i])
				filterArr.push(arr[i])
			} else {
				throw new SyntaxError('Ошибка в ключе')
			}
		} catch (e) {
			if (e.name == 'SyntaxError') {
				console.error('EROR')
			}
		}
	}
}

checking(books)
function createList(arr) {
	arr = arr.map((element) => {
		let newTagLi = document.createElement('li')
		newTagUl.appendChild(newTagLi)
		return (newTagLi.innerHTML = `${element.author} , ${element.name}  ${element.price}`)
	})
}
createList(filterArr)
