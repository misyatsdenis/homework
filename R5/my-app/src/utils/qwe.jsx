export let addCardTo = (array, id) => {
	let obj = array.find((item) => item.id === id)
	return obj
}

export let summBasket = (array) => {
	let total = array.reduce((acc, item) => (acc += item.price), 0)
	return total
}

export let existItemInBasket = (array, id) => {
	return array.some((item) => item.id === id)
}
