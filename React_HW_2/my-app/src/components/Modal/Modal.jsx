import React, { useState } from 'react'
import Button from '../Buttons/Button'
import './Modal.scss'

let Modal = (props) => {
	let { onAddBUYARRAY, RemoveProd } = props

	let Stop = (e) => {
		e.stopPropagation()
	}
	return (
		<div className={`modal ${props.isModalShow ? 'open' : 'close'}`}>
			<div className="overlay" onClick={props.isModalHide}>
				<div
					className="content"
					style={{ background: props.backgroundColor }}
					onClick={Stop}
				>
					<h3>
						{props.header}
						{props.closeButton ? (
							<Button
								text="X"
								backgroundColor="White"
								onClick={props.isModalHide}
							/>
						) : (
							''
						)}
					</h3>

					<p>{props.text}</p>

					<div className="btn-container">
						<Button
							onClick={(e) => {
								props.onClick(e)
								props.isModalHide()
							}}
							className="modal-btn ok"
							text="Ok"
							backgroundColor="green"
							color="yellow"
						/>
						<Button
							onClick={props.isModalHide}
							className="modal-btn Cancel"
							text="Cancel"
							backgroundColor="red"
							color="yellow"
						/>
					</div>
				</div>
			</div>
		</div>
	)
}

export default Modal
