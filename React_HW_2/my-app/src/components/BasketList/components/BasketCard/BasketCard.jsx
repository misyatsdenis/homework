import React from 'react'
import { FaTrashAlt } from 'react-icons/fa'
import Modal from '../../../Modal/Modal'

import './BasketCard.scss'

let BasketCard = (props) => {
	let { item, RemoveProd, Basket } = props

	let [modal, setmodalBuy] = React.useState(false)
	let show = () => {
		setmodalBuy(true)
	}

	return (
		<React.Fragment>
			<div className="basket-card" id={item.id}>
				<div className="wrap">
					<div className="content">
						<div className="img-wrap">
							<img src={item.imageUrl}></img>
						</div>

						<div className="info">
							<p>{item.info}</p>
						</div>
						<div className="addRemoveBtn-wrap">
							<div className="addRemoveBtn">
								<button className="addQti">+</button>
								<input type="text"></input>
								<button className="removeQti">-</button>
							</div>

							<div className="price-wrap">
								<p className="price-curent">
									Unit Price: {item.price} $
								</p>
								<p className="price-total">
									Total Price: <span>{item.price} </span>$
								</p>
							</div>
						</div>
						<div className="button-trash">
							<FaTrashAlt
								onClick={show}
								// onClick={RemoveProd}
								className="trashIcon"
							/>
						</div>
					</div>
				</div>
			{modal ? (
				<Modal
					isModalShow={modal}
					isModalHide={() => setmodalBuy(false)}
					closeButton={true}
					backgroundColor="linear-gradient(to top, #8e0e00, #1f1c18)"
					header="Remove from the basket?"
					text={item.name}
					onClick={RemoveProd}
					// itemID={event.target.closest('.basket-card').id}
					// onAddBUYARRAY={onAddBUYARRAY}
				/>
			) : (
				<></>
			)}
			</div>
		</React.Fragment>
	)
}

export default BasketCard
