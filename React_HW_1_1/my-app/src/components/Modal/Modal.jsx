import React, { useState } from 'react'
import Button from '../Buttons/Button'
import './Modal.scss'

let Modal = (props) => {
	let Stop = (e) => {
		e.stopPropagation()
		console.log('Stop s')
	}

	return (
		<div className={`modal ${props.isModalShow ? 'open' : 'close'}`}>
			<div className="overlay" onClick={props.isModalHide}>
				<div
					className="modal-body"
					style={{ background: props.backgroundColor }}
					onClick={Stop}
				>
					<h3>
						{props.header}
						{props.closeButton ? (
							<Button text="X" backgroundColor="White" />
						) : (
							''
						)}
					</h3>

					<p>{props.text}</p>
					<div>
						<Button
							onClick={props.isModalHide}
							className="modal-btn ok"
							text="Ok"
							backgroundColor="White"
						/>
						<Button
							onClick={props.isModalHide}
							className="modal-btn Cancel"
							text="Cancel"
							backgroundColor="White"
						/>
					</div>
				</div>
			</div>
		</div>
	)
}

export default Modal
