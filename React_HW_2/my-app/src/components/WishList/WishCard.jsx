import React, { useEffect, useState } from 'react'
import { AiFillStar } from 'react-icons/ai'
import './WishCard.scss'

let WishCard = (props) => {
	let { item, RemoveProd } = props

	return (
		<div className="wish-card" id={item.id}>
			<div className="wish-top-content">
				<div className="wish-image_wrap">
					<img src={item.imageUrl} className="wish-image" />
				</div>
				<div className="wish-star">
					<a className="nav-item-link" href="#">
						<AiFillStar
							className="star-icon"
							onClick={RemoveProd}
						/>
					</a>
					<h3 className="aside-text">Dota2</h3>
				</div>
			</div>
			<div className="info">
				<h3>
					<span>Name hero </span>
					<br />
					{item.name}
				</h3>
				<p className="price">
					<span>Cost:</span> {item.price} UAH
				</p>
			</div>

			{/* <Button text="Add to cart" onClick={show} color="red" />
			<React.Fragment>
				<Modal
					isModalShow={modalBuy}
					isModalHide={show}
					closeButton={true}
					backgroundColor="linear-gradient(to top, #8e0e00, #1f1c18)"
					header="Do you want to buy a product?"
					text={props.name}
					// BUYARRAY={BUYARRAY}
					onAddBUYARRAY={onAddBUYARRAY}
				/>
			</React.Fragment> */}
		</div>
	)
}
export default WishCard
