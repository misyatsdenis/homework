import types from './types'

let initialState = JSON.parse(localStorage.getItem('redux')) || {
	products: [],
	basket: [],
	favorites: [],
	buyModal: {
		isOpen: false,
		type: null,
		title: null,
		info: null,
	},

	// deleteModal: {
	// 	isOpen: false,
	// 	title: null,
	// 	info: null,
	// },
}

function reducer(state = initialState, action) {
	switch (action.type) {
		case types.GET_PRODUCT_SUCCESS:
			return {
				...state,
				products: action.payload,
			}
		case types.SET_PRODUCT_TO_BASKET:
			return {
				...state,
				basket: [...state.basket, action.payload],
			}
		case types.REMOVE_PRODUCT_FROM_BASKET:
			return {
				...state,
				basket: state.basket.filter(
					(item) => item.id !== action.payload
				),
			}
		case types.SET_PRODUCT_TO_FAVORITE:
			return {
				...state,
				favorites: state.favorites.some(
					(item) => item.id === action.payload.id
				)
					? state.favorites.filter(
							(item) => item.id !== action.payload.id
					  )
					: [...state.favorites, action.payload],
				products: state.products.map((item) =>
					item.id === action.payload.id
						? { ...item, isFavorite: !item.isFavorite }
						: item
				),
			}

		case types.OPEN_BUY_MODAL:
			return {
				...state,
				buyModal: action.payload,
			}
		case types.CLOSE_BUY_MODAL:
			return {
				...state,
				buyModal: {
					isOpen: false,
					type: null,
					title: null,
					info: null,
				},
			}
		// case types.OPEN_DELETE_MODAL:
		// 	return {
		// 		...state,
		// 		deleteModal: action.payload,
		// 	}
		// case types.CLOSE_DELETE_MODAL:
		// 	return {
		// 		...state,
		// 		deleteModal: {
		// 			isOpen: false,
		// 			title: null,
		// 			info: null,
		// 		},
		// 	}

		default:
			return {
				...state,
			}
	}
}

export default reducer
