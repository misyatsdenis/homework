// let preprocessor = 'sass', // Preprocessor (sass, less, styl); 'sass' also work with the Scss syntax in blocks/ folder.
// 	fileswatch = 'html,htm,txt,json,md,woff2' // List of files extensions for watching & hard reload

const gulp = require('gulp')
const browserSync = require('browser-sync').create()
const sass = require('gulp-sass')(require('sass'))
// const scss = require('gulp-scss')
// const sassglob = require('gulp-sass-glob')
const cleancss = require('gulp-clean-css')
const autoprefixer = require('gulp-autoprefixer')
const imagemin = require('gulp-imagemin')
const minifyjs = require('gulp-js-minify')
const cssMinify = require('gulp-css-minify')
const uglify = require('gulp-uglify')
const clean = require('gulp-clean')
const concat = require('gulp-concat')

gulp.task('buildCSS', function () {
	return (
		gulp
			.src('src/scss/main.scss')
			.pipe(sass().on('error', sass.logError))
			// .pipe(purify(['./public/app/**/*.js', './public/**/*.html'])
			.pipe(autoprefixer({ cascade: false }))
			.pipe(cleancss())
			.pipe(cssMinify())
			.pipe(concat('main.css'))
			.pipe(gulp.dest('dist/css'))
	)
})

gulp.task('JS', function () {
	return (
		gulp
			.src('src/js/**/*.js')
			// .pipe(minifyjs())
			.pipe(gulp.dest('dist'))
	)
})

gulp.task('clean', function () {
	return gulp.src('dist', { allowEmpty: true }, { read: false }).pipe(clean())
})

gulp.task('watchCSS', function () {
	return gulp.watch('src/scss/**/*.scss', gulp.series('buildCSS'))
})
gulp.task('watchJS', function () {
	return gulp.watch('src/js/**/*.js', gulp.series('JS'))
})
gulp.task('watchImg', function () {
	return gulp.watch('src/img/**/*', gulp.series('imgMin'))
})

gulp.task('watch', gulp.parallel(['watchCSS', 'watchJS', 'watchImg']))

gulp.task('browser-sync', function () {
	browserSync.init({ server: { baseDir: './' } })
})

gulp.task('imgMin', () => {
	return gulp
		.src('src/img/**/*')
		.pipe(imagemin())
		.pipe(gulp.dest('dist/images'))
})

gulp.task(
	'build',
	gulp.series(['clean', gulp.parallel(['buildCSS', 'JS', 'imgMin'])])
)
gulp.task('dev', gulp.parallel(['watch', 'browser-sync']))
