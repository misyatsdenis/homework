import React from 'react'
import ReactDom from 'react-dom'
import { useSelector } from 'react-redux'

import BuyModal from './buyModal/buyModal'
import DeleteModal from './deleteModal/deleteModal'
import FormModal from './formModal/formModal'
import OrderModal from './orderModal/orderModal'

import './Modal.scss'

let Modal = () => {
	let Modal = useSelector((state) => state.Modal)

	return ReactDom.createPortal(
		<>
			<>{Modal.type === 'BUY_MODAL' ? <BuyModal /> : null}</>
			<>{Modal.type === 'FORM_MODAL' ? <FormModal /> : null}</>
			<>{Modal.type === 'ORDER_MODAL' ? <OrderModal /> : null}</>
			<>{Modal.type === 'DELETE_MODAL' ? <DeleteModal /> : null}</>
		</>,
		document.getElementById('portal')
	)
}
export default Modal
