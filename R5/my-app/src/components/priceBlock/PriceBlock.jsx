import React from 'react'
import { summBasket } from '../../utils/qwe'
import { useSelector, useDispatch } from 'react-redux'
import { FormModal } from '../../store/actions'
import './PriceBlock.scss'

let PriceBlock = () => {
	let dispatch = useDispatch()

	let BasketArray = useSelector((state) => state.basket)

	let openFormModal = () => {
		dispatch(FormModal())
	}

	return (
		<div className="price-container">
			<aside>
				<h2>Summary</h2>
				<div className="items">
					<p>Items</p>
					<p>{BasketArray.length}</p>
				</div>
				<div className="subtotal">
					<p>Subtotal</p>
					<p>{summBasket(BasketArray)} $</p>
				</div>
				<div className="promopcod">
					<p>PromoCode</p>
					<input type="text"></input>
				</div>
				<div className="line"></div>
				<div className="total">
					<p>Total</p>
					<p>{summBasket(BasketArray)} $</p>
				</div>
				<div className="btnBuy">
					<button type="submit" onClick={openFormModal}>
						Buy
					</button>
				</div>
			</aside>
		</div>
	)
}
export default PriceBlock
