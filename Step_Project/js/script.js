//-----------Our services-- + Our Amazing Work----------------////

let elItemList = document.querySelectorAll('.our_services .list .list-item')
for (let i = 0; i <= elItemList.length - 1; i++) {
	let elItem = elItemList[i]
	elItem.addEventListener('click', btnelItemClickHandler1)
}

function btnelItemClickHandler1(event) {
	document.querySelector('.list-item.active').classList.remove('active')
	event.target.classList.add('active')
	document
		.querySelector('.services-description.active')
		.classList.remove('active')
	const curentIndex = event.target.dataset.index
	document
		.getElementById('description-' + curentIndex)
		.classList.add('active')
}
//--------------  Our Amazing Work----------------////
let elItemList2 = document.querySelectorAll(
	'.our_amazing_work .list .list-item'
)
for (let i = 0; i <= elItemList2.length - 1; i++) {
	let elItem2 = elItemList2[i]
	elItem2.addEventListener('click', btnFilter)
}

function btnFilter(event) {
	document.querySelector('.list-item.active').classList.remove('active')
	event.target.classList.add('active')

	let curentName = event.target.dataset.name
	console.log(curentName)
	let elBlickList = document.querySelectorAll('.block')
	console.log(elBlickList)
	for (let i = 0; i <= elBlickList.length - 1; i++) {
		let elBlock = elBlickList[i]
		let elBlockName = elBlock.dataset.name
		console.log(elBlockName)
		if (elBlockName == curentName) {
			elBlock.classList.remove('hide')
		} else if (curentName == 'All') {
			elBlock.classList.remove('hide')
		} else {
			elBlock.classList.add('hide')
		}
	}
}

let elLoadArr = [
	{
		h2: 'creative design',
		imgIcon: 'img/icon.png',
		img: 'img/galary/landing-page5.jpg',
		p: 'Landing pages',
		title: 'LandingPages',
	},
	{
		h2: 'creative design',
		imgIcon: 'img/icon.png',
		img: 'img/galary/landing-page6.jpg',
		p: 'Landing pages',
		title: 'LandingPages',
	},
	{
		h2: 'creative design',
		imgIcon: 'img/icon.png',
		img: 'img/galary/landing-page7.jpg',
		p: 'Landing pages',
		title: 'LandingPages',
	},
	{
		h2: 'creative design',
		imgIcon: 'img/icon.png',
		img: 'img/galary/web-design6.jpg',
		p: 'Web Design',
		title: 'WebDesign',
	},
	{
		h2: 'creative design',
		imgIcon: 'img/icon.png',
		img: 'img/galary/web-design7.jpg',
		p: 'Web Design',
		title: 'WebDesign',
	},
	{
		h2: 'creative design',
		imgIcon: 'img/icon.png',
		img: 'img/galary/wordpress1.jpg',
		p: 'Wordpress',
		title: 'Wordpress',
	},
	{
		h2: 'creative design',
		imgIcon: 'img/icon.png',
		img: 'img/galary/wordpress2.jpg',
		p: 'Wordpress',
		title: 'Wordpress',
	},
	{
		h2: 'creative design',
		imgIcon: 'img/icon.png',
		img: 'img/galary/wordpress3.jpg',
		p: 'Wordpress',
		title: 'Wordpress',
	},
	{
		h2: 'creative design',
		imgIcon: 'img/icon.png',
		img: 'img/galary/wordpress4.jpg',
		p: 'Wordpress',
		title: 'Wordpress',
	},
	{
		h2: 'creative design',
		imgIcon: 'img/icon.png',
		img: 'img/galary/wordpress5.jpg',
		p: 'Wordpress',
		title: 'Wordpress',
	},
	{
		h2: 'creative design',
		imgIcon: 'img/icon.png',
		img: 'img/galary/graphic-design4.jpg',
		p: 'Graphic design',
		title: 'GraphicDesign',
	},
	{
		h2: 'creative design',
		imgIcon: 'img/icon.png',
		img: 'img/galary/wordpress5.jpg',
		p: 'Wordpress',
		title: 'Wordpress',
	},
]
let addMoreBtn = document.getElementById('ourAmazingWorkBtn')
addMoreBtn.addEventListener('click', () => {
	addMoreBtn.innerText = 'Loading...'

	let blockList = document.getElementById('galery')

	setTimeout(function () {
		let blockListNew = elLoadArr.splice(0, 12)
		blockListNew.forEach((block) => {
			let elBlock = document.createElement('div')
			elBlock.classList.add('block')
			elBlock.dataset.name = `${block.title}`
			elBlock.innerHTML = ` <img src=${block.img} alt="#" class="block-img">
            <img src=${block.imgIcon}  alt="#" class="block-icon">
            <h2 class="main-text">${block.h2}</h2>
            <p class="text">${block.p}</p>`
			blockList.append(elBlock)
		})

		addMoreBtn.innerText = ' + Load More'
		if (elLoadArr.length === 0) {
			addMoreBtn.style.display = 'none'
		}
	}, 2000)
})

////////////////////////////////////////СЛАЙДЕР/////////////////
let SmallPfoto = document.getElementsByClassName('SmallPfotoAutor'),
	SmallPfotoArea = document.getElementById('slider'),
	slides = document.getElementsByClassName('review'),
	prev = document.getElementById('btnPrev'),
	next = document.getElementById('btnNext'),
	slideIndex = 0

prev.addEventListener('click', () => {
	moveSlides(-1)
})

next.addEventListener('click', () => {
	moveSlides(+1)
})

function moveSlides(n) {
	showSlides((slideIndex += n))
}

function currentSlide(n) {
	showSlides((slideIndex = n))
}
showSlides(slideIndex)

function showSlides(n) {
	if (n < 0) {
		slideIndex = slides.length - 1
	} else if (n > slides.length - 1) {
		slideIndex = 0
	}
	for (let i = 0; i < slides.length; i++) {
		slides[i].style.display = 'none'
	}
	for (let i = 0; i < SmallPfoto.length; i++) {
		SmallPfoto[i].classList.remove('up')
	}
	slides[slideIndex].style.display = 'flex'
	SmallPfoto[slideIndex - 0].classList.add('up')
}

SmallPfotoArea.onclick = function (e) {
	for (let i = 0; i < SmallPfoto.length; i++) {
		if (e.target == SmallPfoto[i]) {
			currentSlide(i)
		}
	}
}
// let slides = document.querySelectorAll('.reviews .review')
// let currentSlide = 0
// let btnPrev = document.getElementById('btnPrev')
// let btnNext = document.getElementById('btnNext')

// btnPrev.addEventListener('click', showPrevious)
// btnNext.addEventListener('click', showNext)

// function showPrevious() {
// 	slides[currentSlide].className = 'review'
// 	currentSlide = currentSlide - 1
// 	if (currentSlide < 0) {
// 		currentSlide = slides.length - 1
// 	} else if (currentSlide > slides.length - 1) {
// 		currentSlide = 0
// 	}
// 	slides[currentSlide].className = 'review active'
// 	up()
// }

// function showNext() {
// 	slides[currentSlide].className = 'review'
// 	currentSlide = currentSlide + 1
// 	if (currentSlide < 0) {
// 		currentSlide = slides.length - 1
// 	} else if (currentSlide > slides.length - 1) {
// 		currentSlide = 0
// 	}
// 	slides[currentSlide].className = 'review active'
// 	up()
// }
// function up() {
// 	let activ = document.getElementsByClassName('review active')
// 	let activEl = activ[0]
// 	let activElName = activEl.dataset.name
// 	let elAutorList = document.querySelectorAll('.SmallPfotoAutor')
// 	for (let i = 0; i <= elAutorList.length - 1; i++) {
// 		let elAutor = elAutorList[i]
// 		let elAutorName = elAutor.dataset.name
// 		if (activElName == elAutorName) {
// 			elAutor.classList.add('up')
// 		} else {
// 			elAutor.classList.remove('up')
// 		}
// 	}
// }
// let SmallPfotoAutorList = document.querySelectorAll('.SmallPfotoAutor')
// for (let i = 0; i <= SmallPfotoAutorList.length - 1; i++) {
// 	let Smallphoto = SmallPfotoAutorList[i]
// 	Smallphoto.addEventListener('click', SliderPhotoClick)
// }

// function SliderPhotoClick(event) {
// 	document.querySelector('.SmallPfotoAutor.up').classList.remove('up')
// 	event.target.classList.add('up')
// 	document.querySelector('.review.active').classList.remove('active')
// 	let curentName = event.target.dataset.name
// 	console.log(curentName)
// 	document.getElementById(curentName).classList.add('active')
// }

//-----------------------------------------стрелка на верх////////*****/////
$(document).ready(function () {
	$(window).scroll(function () {
		let height = document.documentElement.clientHeight
		if ($(document.documentElement).scrollTop() > height) {
			$('#button-up').fadeIn()
		} else {
			$('#button-up').fadeOut()
		}
	})
	$('#button-up').click(function () {
		$('html, body').animate(
			{
				scrollTop: 0,
			},
			2000
		)
	})
})
