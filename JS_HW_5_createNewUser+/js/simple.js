function createNewUser (){
  
  return {
      firstName: prompt('Ваше имя:'),
      lastName:  prompt('Ваша Фамилия:'),
      birthday:  prompt('Дата рождения (в формате dd.mm.yyyy):'),

      getLogin(){
        return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },

      getAge(){
        let getCurentDate = Date.now();
        let userDate = Date.parse(this.birthday)
        let userAge = getCurentDate - userDate;
        return Math.floor(userAge / 31536000000);
      },

      getFullage(){
        let userDate = new Date(this.birthday)
        let userYear = userDate.getFullYear()
        return userYear;
      },

      getPassword(){
        return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.getFullage()
      }
    
  }
}
let newUser = createNewUser();
console.log('Логин: ' + newUser.getLogin());
console.log('Возраст: ' + newUser.getAge())
console.log('Пароль: ' + newUser.getPassword())
