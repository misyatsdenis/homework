import React from 'react'
import ReactDom from 'react-dom'
import { useSelector, useDispatch } from 'react-redux'
import {
	setProguctToBasket,
	removeProguctFromBasket,
	closeByuModal,
} from '../../store/actions'
import Button from '../button/Button'
import './Modal.scss'

let Modal = () => {
	let buyModal = useSelector((state) => state.buyModal)
	let deleteModal = useSelector((state) => state.deleteModal)

	let dispatch = useDispatch()

	let closeModal = () => {
		dispatch(closeByuModal())
	}

	let addToBasket = () => {
		dispatch(setProguctToBasket(buyModal.info))
		dispatch(closeByuModal())
	}

	let RemoveProd = (e) => {
		dispatch(removeProguctFromBasket(buyModal.info.id))
		dispatch(closeByuModal())
	}

	// if (!deleteModal.isOpen) return null

	// Modal => to portal 


	// constiner product list  

	// <Modal>
	// 	{if ? content 1 : content 2}
	// </<Modal>
	

	if (buyModal.isOpen && buyModal.type == 'BUY_MODAL') {
		return ReactDom.createPortal(
			<div
				className="modal-container"
				onClick={() => {
					closeModal()
				}}
			>
				<div className="modal" onClick={(e) => e.stopPropagation()}>
					<div className="modal-header">
						<h3>{buyModal.title}</h3>
					</div>
					<div className="modal-body">
						<p>{buyModal.info.name}</p>
					</div>
					<div className="modal-btn-content">
						<Button onClick={addToBasket}>
							Да <br /> хочу купить
						</Button>
						<Button
							onClick={() => {
								closeModal()
							}}
						>
							Нет <br /> пожалуй откажусь
						</Button>
					</div>
				</div>
			</div>,
			document.getElementById('portal')
		)
	} else if (buyModal.isOpen && buyModal.type == 'DELETE_MODAL') {
		return ReactDom.createPortal(
			<div
				className="modal-container"
				onClick={() => {
					closeModal()
				}}
			>
				<div className="modal" onClick={(e) => e.stopPropagation()}>
					<div className="modal-header">
						<h3>{buyModal.title}</h3>
					</div>
					<div className="modal-body">
						<p>{buyModal.info.name}</p>
					</div>
					<div className="modal-btn-content">
						<Button onClick={RemoveProd}>
							Да <br /> УДАЛЯЙ
						</Button>
						<Button
							onClick={() => {
								closeModal()
							}}
						>
							Нет <br /> пожалуй пускай еще полежит
						</Button>
					</div>
				</div>
			</div>,
			document.getElementById('portal')
		)
	} else if (!buyModal.isOpen) {
		return null
	}
}
export default Modal
