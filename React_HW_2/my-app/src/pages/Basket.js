import React, { useEffect, useState } from 'react'
import BasketList from '../components/BasketList/BasketList'
import PriceBlock from '../components/BasketList/components/PriceBlock/PriceBlock'

let BasketPage = () => {
	let [BasketArray2, setBasketArray2] = useState(
		JSON.parse(localStorage.getItem('BUYARRAY'))
	)
	let [Basket, setBasket] = useState(BasketArray2 ? BasketArray2 : [])

	let RemoveProd = (event) => {
		let removeCardId = event.target.closest('.basket-card').id
		console.log('removeCardId', removeCardId)
		let removeCarIndex = Basket.findIndex(function (element) {
			return element.id == removeCardId
		})
		console.log('removeCarIndex', removeCarIndex)
		Basket.splice(removeCarIndex, 1)
		console.log('Basket', Basket)
		localStorage.setItem(`BUYARRAY`, JSON.stringify(Basket))
		setBasketArray2(JSON.parse(localStorage.getItem('BUYARRAY')))
	}

	useEffect(() => {
		setBasket(BasketArray2)
	}, [])
	useEffect(() => {}, [Basket])
	return (
		<div className="BasketPage">
			<div className="BasketPage-wrap">
				<div className="product-container">
					<BasketList Basket={Basket} RemoveProd={RemoveProd} />
				</div>
				<PriceBlock Basket={Basket} />
			</div>
		</div>
	)
}
export default BasketPage
