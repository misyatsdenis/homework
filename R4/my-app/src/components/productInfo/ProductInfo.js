import React from 'react'
import Button from '../button/Button'

import { openByuModal } from '../../store/actions'

import { existItemInBasket } from '../../utils/qwe'
import { useSelector, useDispatch } from 'react-redux'
import './ProductInfo.scss'

let ProductInfo = ({ product }) => {
	let dispatch = useDispatch()
	let itemsInBasket = useSelector((state) => state.basket)

	let openModal = () => {
		dispatch(openByuModal(product))
	}

	return (
		<div className="info">
			<h3>
				<span>Name hero </span>
				<br />
				{product.name}
			</h3>
			<p className="price">
				<span>Cost:</span> {product.price} UAH
			</p>
			<p className="color">
				<span>Main color hero: </span>
				{product.color}
			</p>
			{existItemInBasket(itemsInBasket, product.id) ? (
				<Button className="btn-disable" cursor="not-allowed">
					Добавлено в корзину
				</Button>
			) : (
				<Button color="red" product={product} onClick={openModal}>
					Купить
				</Button>
			)}
		</div>
	)
}
export default ProductInfo
