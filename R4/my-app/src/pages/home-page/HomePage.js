import React from 'react'
import {  useDispatch } from 'react-redux'
import { getProduct } from '../../store/actions'
import ProductList from '../../components/productList/ProductList'

import './HomePage.scss'

export let HomePage = () => {
	const dispatch = useDispatch()
	dispatch(getProduct())

	return (
		<div>
			<ProductList />
		</div>
	)
}
