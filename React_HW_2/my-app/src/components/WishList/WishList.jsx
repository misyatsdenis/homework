import React, { useEffect, useState } from 'react'
import WishCard from '../WishList/WishCard'

import './WishList.scss'

let WishList = (props) => {
	let { RemoveProd, Wish } = props

	return (
		<div className="wish-list">
			{Wish.map((item) => (
				<WishCard key={item.id} item={item} RemoveProd={RemoveProd} />
			))}
		</div>
	)
}
export default WishList
