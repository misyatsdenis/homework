import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import Modal from './components/modals/Modal'
import reportWebVitals from './reportWebVitals'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import store from './store/store'

ReactDOM.render(
	<Provider store={store}>
		<BrowserRouter>
			<App />
			<Modal />
		</BrowserRouter>
	</Provider>,
	document.getElementById('root')
)

reportWebVitals()
