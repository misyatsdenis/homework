import React from 'react'
import ProductCard from '../productCard/ProductCard'
import { useSelector } from 'react-redux'

import './ProductList.scss'

let ProductList = () => {
	const productArray = useSelector((state) => state.products)

	return (
		<div className="product-list">
			{productArray.map((product) => (
				<ProductCard product={product} productArray={productArray} key={product.id} />
			))}
		</div>
	)
}
export default ProductList
