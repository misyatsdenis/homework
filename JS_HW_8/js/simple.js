let elForm = document.querySelector('.form')
let elInput = document.getElementById('input')

function getPrice(Number) {
	let elUserPrice = document.createElement('span')
	elUserPrice.innerText = `Текущая цена: ${Number}`
	let elButton = document.createElement('button')
	elButton.innerText = 'x'
	elButton.className = 'remove-item'
	elUserPrice.append(elButton)
	elForm.prepend(elUserPrice)
}
let addHandler = () => {
	let value = elInput.value
	if (value <= 0) {
		let correct = document.createElement('p')
		correct.innerText = 'Please enter correct price'
		elForm.append(correct)
		return
	}
	getPrice(value)
	elInput.style.background = 'green'
}

elInput.addEventListener('blur', addHandler)
