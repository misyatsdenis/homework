import React, { useEffect, useState } from 'react'
import '../ProductList/ProductList.scss'
import ProductCard from './components/ProductCard/ProductCard'

let ProductList = (props) => {
	let { onAddBUYARRAY, products, onAddWISHARRAY } = props

	return (
		<div className="product-list">
			{products.map((item) => (
				<ProductCard
					key={item.id}
					name={item.name}
					price={item.price}
					color={item.color}
					imageUrl={item.imageUrl}
					cardId={item.id}
					products={products}
					onAddBUYARRAY={onAddBUYARRAY}
					onAddWISHARRAY={onAddWISHARRAY}
				/>
			))}
		</div>
	)
}
export default ProductList
