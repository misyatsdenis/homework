let newArray = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']

let parent = document.body
function showList(arr, parent) {
	let elArray = `${arr
		.map(function (item) {
			return `<li>${item}</li>`
		})
		.join('')}`
	parent.insertAdjacentHTML('afterbegin', elArray)
}
showList(newArray, parent)
