class Employee {
	constructor(name, age, salary) {
		this.name = name
		this.age = age
		this.salary = salary
	}
	getName() {
		return this.name
	}
	setName(value) {
		this.name = value
	}
	getAge() {
		return this.age
	}
	setAge(value) {
		this.age = value
	}
	getSalary() {
		return this.salary
	}
	setSalary(value) {
		this.salary = value
	}
}

class Programmer extends Employee {
	constructor(name, age, salary, lang) {
		super(name, age, salary)
		this.lang = lang
	}

	getProgramerSalary() {
		let programmerSalary = this.getSalary()
		return programmerSalary * 3
	}
}
let programmer_JS = new Programmer('Ivan', 20, 350, ['Java Script', 'Coffy'])
let programmer_Pyton = new Programmer('Ann', 15, 100, 'Pyton')
let programmer_Ruby = new Programmer('Anton', 42, 40000, 'Ruby')

console.log(
	programmer_JS,
	programmer_Pyton,
	programmer_Ruby,
	'                  и перемноженая зарплата                ',
	programmer_JS.getProgramerSalary(),
	programmer_Pyton.getProgramerSalary(),
	programmer_Ruby.getProgramerSalary()
)
