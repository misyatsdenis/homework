import React from 'react'
import './Button.scss'

let Button = ({
	className,
	style,
	onClick,
	children,
	color,
	backgroundColor,
	cursor,
}) => {
	return (
		<button
			className={className}
			style={{
				background: backgroundColor,
				color: color,
				cursor: cursor,
			}}
			onClick={onClick}
		>
			{children}
		</button>
	)
}
export default Button
