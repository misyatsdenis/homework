import React from 'react'
import './BasketEmpty.scss'

let BasketEmpty = () => {
	return (
		<div className="basket-card__empty">
			<h2>Корзина пустая</h2>
		</div>
	)
}
export default BasketEmpty
