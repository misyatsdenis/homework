import React, { Component } from 'react'
import './Button.scss'

let Button = (props) => {
	return (
		<button
			className={props.className}
			style={{ background: props.backgroundColor, color: props.color }}
			onClick={props.onClick}
		>
			{props.text}
		</button>
	)
}
export default Button
