$(document).ready(function (event) {
	$('.main-menu_list__item').click(function (event) {
		let id = $(event.target).attr('href').slice(1)
		console.log(id)
		$('html, body').animate(
			{
				scrollTop: $(`#${id}`).offset().top,
			},
			2000
		)
	})
})

$(document).ready(function () {
	$(window).scroll(function () {
		let height = document.documentElement.clientHeight
		if ($(document.documentElement).scrollTop() > height) {
			$('#button-up').fadeIn()
		} else {
			$('#button-up').fadeOut()
		}
	})
	$('#button-up').click(function () {
		$('html, body').animate(
			{
				scrollTop: 0,
			},
			2000
		)
	})
})
$(document).ready(function () {
	$('#Btn-toggle').click(function () {
		$('#OurMostPopularClients').slideToggle()
	})
})
