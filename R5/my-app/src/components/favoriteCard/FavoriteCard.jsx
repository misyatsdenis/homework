import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { AiFillStar } from 'react-icons/ai'
import { existItemInBasket } from '../../utils/qwe'
import { setProguctToFavorite } from '../../store/actions'
import Button from '../button/Button'
import './FavoriteCard.scss'

import { setProguctToBasket } from '../../store/actions'

let FavoriteCard = (props) => {
	let { product } = props
	let dispatch = useDispatch()

	const BasketArray = useSelector((state) => state.basket)

	let addToBasket = () => {
		dispatch(setProguctToBasket(product))
	}

	let toggleFavorite = () => {
		dispatch(setProguctToFavorite(product))
	}

	return (
		<div className="wish-card" id={product.id}>
			<div className="wish-top-content">
				<div className="wish-image_wrap">
					<img src={product.imageUrl} className="wish-image" alt="alt"/>
				</div>
				<div className="wish-star">
					<span className="nav-item-link">
						<AiFillStar
							className="star-icon"
							onClick={toggleFavorite}
						/>
					</span>
					<h3 className="aside-text">Dota2</h3>
				</div>
			</div>
			<div className="info">
				<h3>
					<span>Name hero </span>
					<br />
					{product.name}
				</h3>
				<p className="price">
					<span>Cost:</span> {product.price} UAH
				</p>
			</div>

			<Button color="red" product={product} onClick={addToBasket}>
				{existItemInBasket(BasketArray, product.id)
					? 'Уже в корзине'
					: 'Купить'}
			</Button>

			{/* <React.Fragment>
				<Modal
					isModalShow={modalBuy}
					isModalHide={show}
					closeButton={true}
					backgroundColor="linear-gradient(to top, #8e0e00, #1f1c18)"
					header="Do you want to buy a product?"
					text={props.name}
					// BUYARRAY={BUYARRAY}
					onAddBUYARRAY={onAddBUYARRAY}
				/>
			</React.Fragment> */}
		</div>
	)
}
export default FavoriteCard
