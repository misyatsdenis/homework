import types from './types'
import getProguct from '../components/api/getProguct'

export let getProguctSuccess = (value) => ({
	type: types.GET_PRODUCT_SUCCESS,
	payload: value,
})

export let getProduct = () => (dispatch) => {
	getProguct().then((product) => {
		dispatch(getProguctSuccess(product))
	})
}

export let setProguctToBasket = (value) => ({
	type: types.SET_PRODUCT_TO_BASKET,
	payload: value,
})

export let removeProguctFromBasket = (value) => ({
	type: types.REMOVE_PRODUCT_FROM_BASKET,
	payload: value,
})
export let setProguctToFavorite = (value) => ({
	type: types.SET_PRODUCT_TO_FAVORITE,
	payload: value,
})
export let clearBasket = () => ({
	type: types.CLEAR_BASKET,
})

export let ByuModal = (value) => ({
	type: types.OPEN_MODAL,
	payload: {
		isOpen: true,
		type: 'BUY_MODAL',
		info: value,
	},
})
export let closeByuModal = () => ({
	type: types.CLOSE_MODAL,
})

export let DeleteModal = (value) => ({
	type: types.OPEN_MODAL,
	payload: {
		isOpen: true,
		type: 'DELETE_MODAL',
		info: value,
	},
})
export let FormModal = () => ({
	type: types.OPEN_MODAL,
	payload: {
		isOpen: true,
		type: 'FORM_MODAL',
		info: null,
	},
})
export let orderModal = (value) => ({
	type: types.OPEN_MODAL,
	payload: {
		isOpen: true,
		type: 'ORDER_MODAL',
		info: value,
	},
})
