import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { removeProguctFromBasket, closeByuModal } from '../../../store/actions'
import Button from '../../button/Button'

let DeleteModal = () => {
	let DeleteModal = useSelector((state) => state.Modal)

	let dispatch = useDispatch()

	let closeModal = () => {
		dispatch(closeByuModal())
	}

	let RemoveProd = () => {
		dispatch(removeProguctFromBasket(DeleteModal.info.id))
		dispatch(closeByuModal())
	}

	return (
		<div
			className="modal-container"
			onClick={() => {
				closeModal()
			}}
		>
			<div className="modal" onClick={(e) => e.stopPropagation()}>
				<div className="modal-header">
					<h3>Точно удалить?</h3>
				</div>
				<div className="modal-body">
					<p>{DeleteModal.info.name}</p>
				</div>
				<div className="modal-btn-content">
					<Button onClick={RemoveProd}>
						Да <br /> УДАЛЯЙ
					</Button>
					<Button
						onClick={() => {
							closeModal()
						}}
					>
						Нет <br /> пожалуй пускай еще полежит
					</Button>
				</div>
			</div>
		</div>
	)
}
export default DeleteModal
