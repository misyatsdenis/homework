import React from 'react'
import BasketCard from '../basketCard/BasketCard'
import { useSelector } from 'react-redux'
import BasketEmpty from '../basketEmpty/BasketEmpty'
import './BasketList.scss'

let BasketList = () => {
	const BasketArray = useSelector((state) => state.basket)

	return (
		<div className="basket-product-list">
			{BasketArray.length > 0 ? (
				BasketArray.map((product) => (
					<BasketCard key={product.id} item={product} />
				))
			) : (
				<BasketEmpty />
			)}
		</div>
	)
}
export default BasketList
