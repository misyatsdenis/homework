let slides = document.querySelectorAll('#slides .slide')
let currentSlide = 0
let slideInterval = setInterval(nextSlide, 2000)
let buttonsInterval = setTimeout(X, 3000)
let playing = true
let pauseButton = document.getElementById('pause')
let playButton = document.getElementById('play')
pauseButton.addEventListener('click', pauseSlideshow)
playButton.addEventListener('click', playSlideshow)

function nextSlide() {
	slides[currentSlide].className = 'slide'
	currentSlide = (currentSlide + 1) % slides.length
	slides[currentSlide].className = 'slide showing'
}
function X() {
	pauseButton.classList.remove('hide')
}

function pauseSlideshow() {
	playing = false
	clearInterval(slideInterval)
	pauseButton.classList.add('hide')
	playButton.classList.remove('hide')
}

function playSlideshow() {
	playing = true
	slideInterval = setInterval(nextSlide, 2000)
	pauseButton.classList.remove('hide')
	playButton.classList.add('hide')
}
