class Table {
	constructor(place, cols, row) {
		this.place = place
		this.cols = cols
		this.row = row
	}
	init() {
		this.createTable()
		this.ActiveBox()
	}
	createTable() {
		let parentTable = document.querySelector(`.${this.place}`)
		let table = document.createElement('table')
		table.classList.add('bigTable')
		for (let i = 0; i < this.row; i++) {
			let tr = document.createElement('tr')
			tr.classList.add('tabRow')
			for (let j = 0; j < this.cols; j++) {
				let td = document.createElement('td')
				td.classList.add('tabCol')
				td.classList.add('green')
				tr.appendChild(td)
			}
			table.appendChild(tr)
		}
		parentTable.appendChild(table)
	}
	ActiveBox() {
		let boxList = document.querySelectorAll('.tabCol ')

		let box = Math.floor(Math.random() * boxList.length)
		this.FriseBox()
		boxList[box].classList.add('active')
	}
	FriseBox() {
		let boxList = document.querySelectorAll('.tabCol ')
		for (let i = 0; i < boxList.length; i++) {
			let box = boxList[i]
			return box
		}
		if (box.classList.contain('freze')) {
			return
		} else {
			box.classList.add('freze')
		}
	}
}

let table1 = new Table('elem', 5, 5).init()

class Player {
	constructor(name, role, color) {
		this.name = name
		this.role = role
		this.color = color
	}
	init() {
		this.Counter()
		this.showResult()
		this.congratulation()
	}
	Counter() {
		let counterList = document.querySelectorAll(`.${this.color}`)
		let count = counterList.length
		return count
	}
	showResult() {
		let result = document.getElementById(`counter-${this.role}`)
		result.innerHTML = this.Counter()
	}
	congratulation() {
		let winer = document.querySelectorAll(`.${this.color}`)
		if (winer.length > winer.length / 2) {
			console.log(`${this.name} winer`)
		}
	}
}
let userPlayer = new Player('Den', 'user', 'green').init()
let pcPlayer = new Player('Compucter', 'PC', 'red').init()
