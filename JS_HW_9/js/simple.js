let tabsList = document.querySelectorAll('.tabs .tabs-title')
for (i = 0; i <= tabsList.length - 1; i++) {
	let tabsListItem = tabsList[i]
	tabsListItem.addEventListener('click', handler)
}

function handler(event) {
	document.querySelector('.tabs-title.active').classList.remove('active')
	event.target.classList.add('active')
	document.querySelector('.tab-content.active').classList.remove('active')
	let curentIndex = event.target.dataset.index
	document.getElementById('tab-' + curentIndex).classList.add('active')
}
