import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { closeByuModal } from '../../../store/actions'
import './orderModal.scss'

let OrderModal = () => {
	let orderModal = useSelector((state) => state.Modal.info)

	let dispatch = useDispatch()
	let closeModal = () => {
		dispatch(closeByuModal())
	}
	return (
		<div
			className="modal-order-container"
			onClick={() => {
				closeModal()
			}}
		>
			<div className="modal-order" onClick={(e) => e.stopPropagation()}>
				<div className="modal-order-title">
					<h2>Поздравляем с покупкой </h2>
					<p>{orderModal.firstName}</p>
				</div>
				<div className="modal-order-info">
					<h4>Ваши реквизиты:</h4>
					<ul>
						<li>
							<div>Ваше имя</div>
							<div> {orderModal.firstName}</div>
						</li>
						<li>
							<div>Ваша фамилия</div>
							<div>{orderModal.lastName}</div>
						</li>
						<li>
							<div>Ваш возраст</div>
							<div>{orderModal.age}</div>
						</li>
						<li>
							<div>Ваш номер телефона</div>
							<div>{orderModal.telephone}</div>
						</li>
						<li>
							<div>Ваш адрес</div>
							<div>{orderModal.adress}</div>
						</li>
					</ul>
				</div>
				<div className="modal-order-footer">
					<h2>Спасибо за покупку </h2>
					<p>{orderModal.firstName}</p>
				</div>
			</div>
		</div>
	)
}

export default OrderModal
