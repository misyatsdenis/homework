import types from './types'
import getProguct from '../components/api/getProguct'

export let getProguctSuccess = (value) => ({
	type: types.GET_PRODUCT_SUCCESS,
	payload: value,
})

export let getProduct = () => (dispatch) => {
	getProguct().then((product) => {
		dispatch(getProguctSuccess(product))
	})
}

export let setProguctToBasket = (value) => ({
	type: types.SET_PRODUCT_TO_BASKET,
	payload: value,
})

export let removeProguctFromBasket = (value) => ({
	type: types.REMOVE_PRODUCT_FROM_BASKET,
	payload: value,
})
export let setProguctToFavorite = (value) => ({
	type: types.SET_PRODUCT_TO_FAVORITE,
	payload: value,
})

export let openByuModal = (value) => ({
	type: types.OPEN_BUY_MODAL,
	payload: {
		isOpen: true,
		type: 'BUY_MODAL',
		title: 'Добавить товар в корзину?',
		info: value,
	},
})
export let closeByuModal = () => ({
	type: types.CLOSE_BUY_MODAL,
})

export let openDeleteModal = (value) => ({
	type: types.OPEN_BUY_MODAL,
	payload: {
		isOpen: true,
		type: 'DELETE_MODAL',
		title: 'Удалить товар из корзины?',
		info: value,
	},
})
export let closeByuModal2 = () => ({
	type: types.CLOSE_DELETE_MODAL,
})
