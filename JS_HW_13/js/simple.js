let ThemIMG = document.getElementById('ThemIMG')
let Themlink = document.getElementById('Themlink')
ThemIMG.addEventListener('click', ChangeTheme)
let localS = localStorage.getItem('curentTheme')
Themlink.setAttribute('href', localS)

function ChangeTheme() {
	let mainTeam = 'css/style.css'
	let darkTeam = 'css/dark.css'
	let currTheme = Themlink.getAttribute('href')
	console.log(currTheme)
	let theme = ''
	if (currTheme == darkTeam) {
		currTheme = mainTeam
	} else {
		currTheme = darkTeam
	}
	Themlink.setAttribute('href', currTheme)
	localStorage.setItem('curentTheme', Themlink.getAttribute('href'))
}
